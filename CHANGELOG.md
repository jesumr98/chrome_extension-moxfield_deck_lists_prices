# Changelog

All notable changes to this project will be documented in this file.

## [1.0] - 01-01-2024

Initial commit

### Added

- Initial release of the Moxfield Price Extension.

[1.0]: https://gitlab.com/jesumr98/chrome_extension-moxfield_deck_lists_prices/-/tree/1.0?ref_type=heads

## [1.1] - 01-01-2024

Added documentation

### Added

- Documentation
- Increase version

[1.1]: https://gitlab.com/jesumr98/chrome_extension-moxfield_deck_lists_prices/-/tree/1.0?ref_type=heads