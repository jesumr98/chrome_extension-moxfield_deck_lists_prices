let injectedTabs = {};

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  if (changeInfo.status === 'complete' && /^http/.test(tab.url)) {
    if (!injectedTabs[tabId]) {
      chrome.scripting.executeScript({
        target: { tabId: tabId },
        files: ["./modDecks.js"]
      })
      .then(() => {
        injectedTabs[tabId] = true;
      })
      .catch(err => console.log(err));
    }
  }
});

chrome.tabs.onRemoved.addListener((tabId) => {
  delete injectedTabs[tabId];
});