let observer = new MutationObserver((mutations) => {
    let sidebar = document.querySelector(".flex-shrink-0");
    if (sidebar) {
        sidebar.remove();
    }

    if (!Array.from(document.querySelectorAll("th")).find(th => th.textContent === "Price")) {
        let colorsTh = Array.from(document.querySelectorAll("th")).find(th => th.textContent === "Colors");
        if (colorsTh) {
            let newTh = document.createElement("th");
            newTh.textContent = "Price";
            colorsTh.parentNode.insertBefore(newTh, colorsTh);
        }

        let rows = document.querySelectorAll('tr.cursor-grab');
        rows.forEach(row => {
            let newTd = document.createElement('td');
            let firstTd = row.querySelector('td');
            if (firstTd) {
                firstTd.parentNode.insertBefore(newTd, firstTd.nextSibling);
                let link = firstTd.querySelector('a[href*="/decks/"]');
                if (!link) {
                    return;
                    
                }

                let urlDeckApi = link.href.replace("www.moxfield.com/decks", "api2.moxfield.com/v3/decks/all");

                fetch(urlDeckApi)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(`HTTP error! status: ${response.status}`);
                        } else if (response.headers.get('content-type').includes('application/json')) {
                            return response.json();
                        } else {
                            throw new Error('La respuesta no es JSON');
                        }
                    })
                    .then(data => {
                        console.log(data);
                        let totalPrice = 0;
                        Object.values(data.boards.mainboard.cards).forEach(card => {
                            if (card.card.prices.eur != null) {
                                let price = card.quantity * card.card.prices.eur;
                                totalPrice += price;
                            }
                        });
                        Object.values(data.boards.sideboard.cards).forEach(card => {
                            if (card.card.prices.eur != null) {
                                let price = card.quantity * card.card.prices.eur;
                                totalPrice += price;
                            }
                        });
                        Object.values(data.boards.commanders.cards).forEach(card => {
                            if (card.card.prices.eur != null) {
                                totalPrice += card.card.prices.eur;
                            }
                        });
                        newTd.textContent = totalPrice.toFixed(2) + " €";
                    })
                    .catch(error => console.error('Error:', error));
            }
        });
    }

});

observer.observe(document, {
    childList: true,
    subtree: true
});