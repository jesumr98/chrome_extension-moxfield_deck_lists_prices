# Contributing to Moxfield Price Extension

We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features

## We Develop with Gitlab

We use Gitlab to host code, to track issues and feature requests, as well as accept pull requests.

## We Use Gitlab, So All Code Changes Happen Through Pull Requests

Pull requests are the best way to propose changes to the codebase. We actively welcome your pull requests:

1. Fork the repo and create your branch from `main`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code lints.
6. Issue that pull request!

## Any contributions you make will be under the MIT Software License

In short, when you submit code changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.

## Report bugs using Gitlab's [issues](https://gitlab.com/jesumr98/chrome_extension-moxfield_deck_lists_prices/-/issues)

We use Gitlabd issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/jesumr98/chrome_extension-moxfield_deck_lists_prices/-/issues); it's that easy!

## Write bug reports with detail, background, and sample code

Great Bug Reports tend to have:

- A quick summary and/or background
- Steps to reproduce
- Be specific!
- Give sample code if you can.
- What you expected would happen
- What actually happens

## Use a Consistent Coding Style

* 4 spaces for indentation rather than tabs

## License

By contributing, you agree that your contributions will be licensed under its MIT License.

## References

This document was adapted from the open-source contribution guidelines for [Facebook's Draft](https://github.com/facebook/draft-js)