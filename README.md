# Moxfield Price Extension

## Description

This Chrome extension adds the price of cards on Moxfield. It's designed to enhance your experience on Moxfield by providing additional information about the cards.

## Installation

Follow these steps to install the extension:

1. Open the Google Chrome browser on your computer.
2. Navigate to `chrome://extensions`.
3. Enable Developer Mode by clicking the toggle switch next to "Developer mode".
4. Click the "Load unpacked" button and select the extension directory.

## Usage

After installing the extension, navigate to Moxfield. The price of the cards will be automatically displayed.

## Contributing

If you want to contribute to this project, please submit a pull request or open an issue for discussion.

## License

This project is licensed under the MIT License. See the LICENSE file for details.